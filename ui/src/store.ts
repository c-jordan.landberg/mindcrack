import { compose, applyMiddleware, createStore, Store } from "redux";
import reduxThunk from "redux-thunk";
import logger from "redux-logger";
import { state } from "./reducers";


const a: any = window;
const componseEnhancers = a.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = componseEnhancers(
    applyMiddleware(reduxThunk, logger),
    // other store enhancers if any
);

export const store: Store<any> = createStore(
    state,
    enhancer
)