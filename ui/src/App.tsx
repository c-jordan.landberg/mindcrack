import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.scss';
import NotFound from './components/not-found/not-found.component';
import Main from './components/main-page/main.component';
import ImgLoad from './components/img-load/img-load.component';
import { Provider } from 'react-redux';
import { store } from './store';
import ImgDev from './components/dev/img-load-dev.component';
import OdnDev from './components/dev/odn-load-dev.component';
import SgDev from './components/dev/sg-load-dev.component';
import SgLoad from './components/img-load/sg-load/sg-load.component';
import OdnLoad from './components/img-load/odn-load/odn-load.component';
import HistoryDev from './components/dev/history-list-dev.component';
import HistoryLoadDev from './components/dev/history-load-dev.component';
import { NavBar } from './components/nav/nav.component';
import HistoryLoad from './components/history/history-load.component';
import HistoryList from './components/history/history-list.component';
// import { NavBar } from './components/nav/nav.component';s
// import NavbarComponent from './components/navbar/navbar.component';


//import { userInfo } from 'os';


const App: React.FC = () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <div className="App">
                    <NavBar />
                    {/* The switch will only render the first route to match */}
                    <Switch>
                        <Route path="/main" component={Main} />
                        <Route path="/img/sg" component={SgLoad} />
                        <Route path="/img/odn" component={OdnLoad} />
                        <Route path="/img/history" component={HistoryLoad} />
                        <Route path="/history" component={HistoryList} />
                        <Route path="/img" component={ImgLoad} />



                        <Route path="/dev/odn" component={OdnDev} />
                        <Route path="/dev/sg" component={SgDev} />
                        <Route path="/dev/img/history" component={HistoryLoadDev} />
                        <Route path="/dev/img" component={ImgDev} />
                        <Route path="/dev/history" component={HistoryDev} />
                        <Route path="" component={NotFound} />
                    </Switch>
                </div>
            </BrowserRouter>
        </Provider>
    );
}

export default App;
