import React, { Component } from 'react'
import { Button } from 'reactstrap';
import ImgDev from './img-load-dev.component';

interface Props {

}
interface State {
    file: any
}

export default class SgDev extends Component<Props, State> {
    state = {
        file: {
            guide_type: [
                {
                    guide_type_name: "",
                    division: [
                        {
                            division_name: "",
                            pass: [
                                {
                                    paid: "",
                                    series_title: "",
                                    ep_title: "",
                                    season_num: "",
                                    ep_num: "",
                                    playback: "",
                                    series_search_presence: "",
                                    ep_search_presence: "",
                                    details: "",
                                    slot: "",
                                    url: "",
                                    error: ""
                                }
                            ],
                            queue: [],
                            fail: [
                                {
                                    paid: "",
                                    series_title: "",
                                    ep_title: "",
                                    season_num: "",
                                    ep_num: "",
                                    playback: "",
                                    series_search_presence: "",
                                    ep_search_presence: "",
                                    details: "",
                                    slot: "",
                                    url: "",
                                    error: ""
                                }
                            ],
                            error: []
                        }
                    ]
                }
            ]
        }
    }

    async componentDidMount() {
        let fileList = require('../../assets/data/fileList.json');
        let file;
        for (let key in fileList) {
            if (key === 'sg') {
                file = require(`../../assets/active-json/${fileList[key]}`)
            }
        }
        this.setState({
            file
        });
    }

    renderImgSG = (passFail: string) => {
        const file = this.state.file.guide_type
        for (let guideKey in file) {
            let guides = file[guideKey];
            if (guides.guide_type_name === 'sg') {
                for (let divisionKey in guides) {
                    if (divisionKey === 'division') {
                        let divisions = guides[divisionKey];
                        console.log(divisions)
                        for (let division in divisions) {
                            let individual = divisions[division];
                            if (passFail === 'pass') {
                                return individual.pass.map(pass =>
                                    <tr>
                                        <td>{pass.paid}{console.log(pass)}</td>
                                        <td>{pass.series_title}</td>
                                        <td>{Boolean(pass.ep_title) ? pass.ep_title : 'N/A'}</td>
                                        <td>{pass.season_num}</td>
                                        <td>{pass.ep_num}</td>
                                        <td>{pass.playback}</td>
                                        <td>{pass.series_search_presence}</td>
                                        <td>{pass.ep_search_presence}</td>
                                        <td>{Boolean(pass.details) ? pass.details : 'N/A'}</td>
                                        <td>{Boolean(pass.slot) ? pass.slot : 'N/A'}</td>
                                        <td id="imgLoad"><img src={pass.url} alt="Missing URL" className="imgLoad" /></td>
                                        <td>{Boolean(pass.error[0]) ? pass.error : 'N/A'}</td>
                                    </tr>
                                )
                            }
                            else if (passFail === 'fail') {
                                return individual.fail.map(fail =>
                                    <tr>
                                        <td>{fail.paid}{console.log(fail)}</td>
                                        <td>{fail.series_title}</td>
                                        <td>{Boolean(fail.ep_title) ? fail.ep_title : 'N/A'}</td>
                                        <td>{fail.season_num}</td>
                                        <td>{fail.ep_num}</td>
                                        <td>{fail.playback}</td>
                                        <td>{fail.series_search_presence}</td>
                                        <td>{fail.ep_search_presence}</td>
                                        <td>{Boolean(fail.details) ? fail.details : 'N/A'}</td>
                                        <td>{fail.slot}</td>
                                        <td id="imgLoad"><img src={fail.url} alt="Missing URL" className="imgLoad" /></td>
                                        <td>{Boolean(fail.error[0]) ? fail.error : 'N/A'}</td>
                                    </tr>
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    show = (passFail: string) => {

        let pass = document.getElementById("pass");
        let fail = document.getElementById("fail");
        if (passFail === 'pass') {
            pass && (pass.style.cssText = "display: block");
            pass && (pass.setAttribute('disabled', 'false'));
            fail && (fail.style.cssText = "display: none");
            fail && (fail.setAttribute('disabled', 'true'));
        }
        else if (passFail === 'fail') {
            pass && (pass.style.cssText = "display: none");
            pass && (pass.setAttribute('disabled', 'true'));
            fail && (fail.style.cssText = "display: block");
            fail && (fail.setAttribute('disabled', 'false'));
        }
    }

    render() {
        return (
            <div className="imgLoadComponent">
                <ImgDev />
                <br />
                <table className="info-table-div">
                    <tr>
                        <th>Spec Guide</th>
                        <td><a href="https://chstlouismo.tmcserver.com/#/pages/test-status">https://chstlouismo.tmcserver.com/#/pages/test-status</a></td>
                    </tr>
                </table>
                <br />
                <Button className="imgPassBtn" color="success" onClick={() => this.show('pass')}>Pass</Button>
                <Button className="imgFailBtn" color="danger" onClick={() => this.show('fail')}>Fail</Button>
                <br />
                <div id="pass">
                    <h1>Pass</h1>
                    <br />
                    <table className="img-table-div">
                        <tr>
                            <th>Paid</th>
                            <th>Series Title</th>
                            <th>Episode Title</th>
                            <th>Season Number</th>
                            <th>Episode Number</th>
                            <th>Playback</th>
                            <th>Series Search Presence</th>
                            <th>Episode Search Presence</th>
                            <th>Details</th>
                            <th>Slot</th>
                            <th>Image</th>
                            <th>Error</th>
                        </tr>
                        {this.renderImgSG('pass') !== [] ? this.renderImgSG('pass') :
                            <tr>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                            </tr>
                        }
                        {/* {console.log(`We are here: ${JSON.stringify(this.renderImgSG('pass'))}`)} */}
                    </table>
                    <br />
                </div>
                <div id="fail">
                    <h1>Fail</h1>
                    <br />
                    <table className="img-table-div">
                        <tr>
                            <th>Paid</th>
                            <th>Series Title</th>
                            <th>Episode Title</th>
                            <th>Season Number</th>
                            <th>Episode Number</th>
                            <th>Playback</th>
                            <th>Series Search Presence</th>
                            <th>Episode Search Presence</th>
                            <th>Details</th>
                            <th>Slot</th>
                            <th>Image</th>
                            <th>Error</th>
                        </tr>
                        {this.renderImgSG('fail') !== [] ? this.renderImgSG('fail') :
                            <tr>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                            </tr>
                        }
                    </table>
                </div>
            </div>
        )
    }
}
