import React, { Component } from 'react'
import { Link } from 'react-router-dom'

interface Props {
    
}

interface State {
    
}

export default class ImgDev extends Component<Props, State> {
    state = {}

    render() {
        return (
            <div className="imgLoadComponent">
                <br/>
                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Guide Type
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <Link to="/dev/odn" className="dropdown-item" >ODN</Link>
                    <Link to="/dev/sg" className="dropdown-item" >SpecGuide</Link>
                </div>
            </div>
        )
    }
}
