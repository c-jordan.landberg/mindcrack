import React, { Component } from 'react';

export default class NotFound extends Component{

    render() {
        return (
            <div>
                <h1>404 Page Not Found</h1>
                <p>If this issue persists, please reach out to ...</p>
            </div>
        )
    }
}
