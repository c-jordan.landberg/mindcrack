import React, { Component } from 'react'
import { Button } from 'reactstrap';
import ImgLoad from '../img-load/img-load.component';
import { RouteComponentProps } from 'react-router';

interface State {
    fileName: string,
    file: any
}

export default class HistoryLoad extends Component<RouteComponentProps, State> {
    state = {
        fileName: "",
        file: {
            guide_type: [
                {
                    guide_type_name: "",
                    division: [
                        {
                            division_name: "",
                            pass: [
                                {
                                    paid: "",
                                    series_title: "",
                                    ep_title: "",
                                    season_num: "",
                                    ep_num: "",
                                    playback: "",
                                    series_search_presence: "",
                                    ep_search_presence: "",
                                    details: "",
                                    slot: "",
                                    url: "",
                                    error: ""
                                }
                            ],
                            queue: [],
                            fail: [
                                {
                                    paid: "",
                                    series_title: "",
                                    ep_title: "",
                                    season_num: "",
                                    ep_num: "",
                                    playback: "",
                                    series_search_presence: "",
                                    ep_search_presence: "",
                                    details: "",
                                    slot: "",
                                    url: "",
                                    error: ""
                                }
                            ],
                            error: []
                        }
                    ]
                }
            ]
        }
    }

    async componentDidMount() {
        const { fileName } = this.props.location.state;
        let historyFile = require(`../../assets/history-json/${fileName}`);

        let odn = document.getElementById('odn');
        odn && (odn.style.display = 'none');

        this.setState({
            fileName,
            file: historyFile
        });
    }

    renderImgHistory = (passFail: string, guideType: string) => {
        const file = this.state.file.guide_type
        let insert;
        if (guideType === 'sg') {
            if (passFail === 'pass') {
                insert = document.getElementById('sg-pass');
            }
            else if (passFail === 'fail') {
                insert = document.getElementById('sg-fail');
            }
        }
        else if (guideType === 'odn') {
            if (passFail === 'pass') {
                insert = document.getElementById('odn-pass');
            }
            else if (passFail === 'fail') {
                insert = document.getElementById('odn-fail');
            }
        }
        for (let guideKey in file) {
            let guides = file[guideKey];
            if (guides.guide_type_name === guideType) {
                for (let divisionKey in guides) {
                    if (divisionKey === 'division') {
                        let divisions = guides[divisionKey];
                        console.log(divisions)
                        for (let division in divisions) {
                            let individual = divisions[division];
                            if (passFail === 'pass') {
                                let x = individual.pass.map(pass =>
                                    `<tr>
                                        <td>${pass.paid}</td>
                                        <td>${pass.series_title}</td>
                                        <td>${Boolean(pass.ep_title) ? pass.ep_title : 'N/A'}</td>
                                        <td>${pass.season_num}</td>
                                        <td>${pass.ep_num}</td>
                                        <td>${pass.playback}</td>
                                        <td>${pass.series_search_presence}</td>
                                        <td>${pass.ep_search_presence}</td>
                                        <td>${Boolean(pass.details) ? pass.details : 'N/A'}</td>
                                        <td>${Boolean(pass.slot) ? pass.slot : 'N/A'}</td>
                                        <td id="imgLoad"><img src="${pass.url}" alt="Missing URL" className="imgLoad" /></td>
                                        <td>${Boolean(pass.error[0]) ? pass.error : 'N/A'}</td>
                                    </tr>`
                                )
                                insert && (insert.innerHTML += x)
                            }
                            else if (passFail === 'fail') {
                                let x = individual.fail.map(fail =>
                                    `<tr>
                                        <td>${fail.paid}</td>
                                        <td>${fail.series_title}</td>
                                        <td>${Boolean(fail.ep_title) ? fail.ep_title : 'N/A'}</td>
                                        <td>${fail.season_num}</td>
                                        <td>${fail.ep_num}</td>
                                        <td>${fail.playback}</td>
                                        <td>${fail.series_search_presence}</td>
                                        <td>${fail.ep_search_presence}</td>
                                        <td>${Boolean(fail.details) ? fail.details : 'N/A'}</td>
                                        <td>${fail.slot}</td>
                                        <td id="imgLoad"><img src="${fail.url}" alt="Missing URL" className="imgLoad" /></td>
                                        <td>${Boolean(fail.error[0]) ? fail.error : 'N/A'}</td>
                                    </tr>`
                                )
                                insert && (insert.innerHTML += x)
                            }
                        }
                    }
                }
            }
        }
    }

    show = (passFail: string, guideType: string) => {
        
        let odn = document.getElementById('odn');
        let sg = document.getElementById('sg');
        if (guideType === 'sg') {
            let pass = document.getElementById('sg-pass-div');
            let fail = document.getElementById('sg-fail-div');
            if (passFail === 'pass') {
                pass && (pass.style.cssText = "display: block");
                fail && (fail.style.cssText = "display: none");
            }
            else if (passFail === 'fail') {
                pass && (pass.style.cssText = "display: none");
                fail && (fail.style.cssText = "display: block");
            }
            else {
                sg && (sg.style.display = 'block');
                odn && (odn.style.display = 'none');
            }
        }
        else if (guideType === 'odn') {
            let pass = document.getElementById('odn-pass-div');
            let fail = document.getElementById('odn-fail-div');
            if (passFail === 'pass') {
                pass && (pass.style.cssText = "display: block");
                fail && (fail.style.cssText = "display: none");
            }
            else if (passFail === 'fail') {
                pass && (pass.style.cssText = "display: none");
                fail && (fail.style.cssText = "display: block");
            }
            else {
                odn && (odn.style.display = 'block');
                sg && (sg.style.display = 'none');
            }
        }
    }

    render() {
        return (
            <div className="imgLoadComponent">
                <Button className="imgPassBtn" color="info" onClick={() => this.show('none', 'odn')}>ODN</Button>
                <Button className="imgFailBtn" color="info" onClick={() => this.show('none', 'sg')}>SG</Button>
                <div id="sg">
                    <table className="info-table-div">
                        <tr>
                            <th>Spec Guide</th>
                        </tr>
                    </table>
                    <br />
                    <Button className="imgPassBtn" color="success" onClick={() => this.show('pass', 'sg')}>Pass</Button>
                    <Button className="imgFailBtn" color="danger" onClick={() => this.show('fail', 'sg')}>Fail</Button>
                    <br />
                    <div id="sg-pass-div">
                        <h1>Pass</h1>
                        <br />
                        <table className="img-table-div" id="sg-pass">
                            <tr>
                                <th>Paid</th>
                                <th>Series Title</th>
                                <th>Episode Title</th>
                                <th>Season Number</th>
                                <th>Episode Number</th>
                                <th>Playback</th>
                                <th>Series Search Presence</th>
                                <th>Episode Search Presence</th>
                                <th>Details</th>
                                <th>Slot</th>
                                <th>Image</th>
                                <th>Error</th>
                            </tr>
                            {this.renderImgHistory('pass', 'sg')}
                        </table>
                        <br />
                    </div>
                    <div id="sg-fail-div">
                        <h1>Fail</h1>
                        <br />
                        <table className="img-table-div" id="sg-fail">
                            <tr>
                                <th>Paid</th>
                                <th>Series Title</th>
                                <th>Episode Title</th>
                                <th>Season Number</th>
                                <th>Episode Number</th>
                                <th>Playback</th>
                                <th>Series Search Presence</th>
                                <th>Episode Search Presence</th>
                                <th>Details</th>
                                <th>Slot</th>
                                <th>Image</th>
                                <th>Error</th>
                            </tr>
                            {this.renderImgHistory('fail', 'sg')}
                        </table>
                    </div>
                </div>
                <div id="odn">
                    <table className="info-table-div">
                        <tr>
                            <th>ODN</th>
                        </tr>
                    </table>
                    <br />
                    <Button className="imgPassBtn" color="success" onClick={() => this.show('pass', 'odn')}>Pass</Button>
                    <Button className="imgFailBtn" color="danger" onClick={() => this.show('fail', 'odn')}>Fail</Button>
                    <br />
                    <div id="odn-pass-div">
                        <h1>Pass</h1>
                        <br />
                        <table className="img-table-div" id="odn-pass">
                            <tr>
                                <th>Paid</th>
                                <th>Series Title</th>
                                <th>Episode Title</th>
                                <th>Season Number</th>
                                <th>Episode Number</th>
                                <th>Playback</th>
                                <th>Series Search Presence</th>
                                <th>Episode Search Presence</th>
                                <th>Details</th>
                                <th>Slot</th>
                                <th>Image</th>
                                <th>Error</th>
                            </tr>
                            {this.renderImgHistory('pass', 'odn')}
                        </table>
                        <br />
                    </div>
                    <div id="odn-fail-div">
                        <h1>Fail</h1>
                        <br />
                        <table className="img-table-div" id="odn-fail">
                            <tr>
                                <th>Paid</th>
                                <th>Series Title</th>
                                <th>Episode Title</th>
                                <th>Season Number</th>
                                <th>Episode Number</th>
                                <th>Playback</th>
                                <th>Series Search Presence</th>
                                <th>Episode Search Presence</th>
                                <th>Details</th>
                                <th>Slot</th>
                                <th>Image</th>
                                <th>Error</th>
                            </tr>
                            {this.renderImgHistory('fail', 'odn')}
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}
