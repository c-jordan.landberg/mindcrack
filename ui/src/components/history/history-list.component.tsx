import React, { Component } from 'react'
import { Link } from 'react-router-dom';

interface Props {

}
interface State {
    history: any
}

export default class HistoryList extends Component<Props, State> {
    state = {
        history: []
    }

    componentDidMount() {
        let json = require('../../assets/data/historyFileList.json');
        this.setState({
            history: json.files
        });
    }

    renderHistoryFiles = () => {
        console.log(this.state.history)
        return this.state.history.map(file =>
            <tr>
                <td><Link to={{
                    pathname: '/img/history',
                    state: {
                        fileName: file
                    }
                }} className="history-link">{file}</Link></td>
            </tr>    
        )
    }

    render() {
        return (
            <div className="style-history">
                <h1>History</h1>
                <br/>
                <table className="history-table-div" id="renderHistoryFiles">
                    <tr>
                        <th>Files</th>
                    </tr>
                    {this.renderHistoryFiles()}
                </table>
            </div>
        )
    }
}
