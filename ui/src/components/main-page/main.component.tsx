import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';

interface State {
    model: string[]
}

export default class Main extends Component<State> {
    state = {
        model: []
    }

    async componentDidMount() {
        const model = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"];
        await this.setState({
            model
        })
    }

    displayHeadends = () => {

        return this.state.model.map(num =>
            <>
                <div className="randomtext">
                    <Button className="dropdown-toggle">{num}</Button>
                </div>
            </>
        );
    }

    displayTMCs = () => {

        return this.state.model.map(num =>
            <>
                <div className="randomtext">
                    <ul className="multi-column-dropdown">
                        <li><a href="#">{num}</a></li>
                        <li><a href="#">{num}</a></li>
                        <li><a href="#">{num}</a></li>
                        <li><a href="#">{num}</a></li>
                        <li><a href="#">{num}</a></li>
                    </ul>
                </div>
            </>
        );
    }

    toggleDropdown = (x: number = 0, e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        e.currentTarget.querySelector("ul")
        e.currentTarget.toggleAttribute("show");
        e.currentTarget.toggleAttribute("hide");
    }

    render() {
        return (
            <div>
                <h1>Main</h1>
                <p>Below model state</p>




                <div className="dropdown">
                    <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Tutorials
                    <span className="caret"></span></button>
                    <ul className="dropdown-menu">
                        <li><Link to="#">HTML</Link></li>
                        <li><Link to="#">CSS</Link></li>
                        <li className="dropdown-submenu">
                            <Link className="test dropdown-toggle" to="#" onClick={(e) => this.toggleDropdown(1, e)}>New dropdown <span className="caret"></span></Link>
                            <ul className="dropdown-menu">
                                <li><Link to="#">2nd level dropdown</Link></li>
                                <li><Link to="#">2nd level dropdown</Link></li>
                                <li className="dropdown-submenu">
                                    <Link className="test" to="#">Another dropdown <span className="caret"></span></Link>
                                    <ul className="dropdown-menu">
                                        <li><Link to="#">3rd level dropdown</Link></li>
                                        <li><Link to="#">3rd level dropdown</Link></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>




                <div className="navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul className="nav navbar-nav">
                        <li className="dropdown">
                            <Button href="#" className="dropdown-toggle" data-toggle="dropdown">Multi Column <b className="caret"></b></Button>
                            <ul className="dropdown-menu multi-column column-length">
                                {this.displayHeadends()}
                            </ul>
                            <ul className="dropdown-menu multi-column column-length">
                                {this.displayTMCs()}
                            </ul>
                        </li>
                    </ul>
                </div>

            </div>
        )
    }
}
