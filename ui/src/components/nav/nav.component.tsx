import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class NavBar extends Component {

    render() {
        return (
            <nav className="navbar navbar-toggleable-lg navbar-expand-lg display-front nav-pad" id="style-navbar">
                <div className="c-pointer shift-left active">
                    <ul className="navbar-nav ml-auto margin-nav">
                        <li className="nav-item" id="dashboard">
                            <h3><Link to="/img/odn" className="unset-anchor nav-link">Current Test</Link></h3>
                        </li>
                    </ul>
                </div>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarsExample04">
                    <ul className="navbar-nav margin-nav">
                        <li className="nav-item" id="dashboard">
                            <Link to="/history" className="unset-anchor nav-link">History</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}