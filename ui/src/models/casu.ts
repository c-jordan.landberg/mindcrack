// Casu log fields
export default class Casu {
    constructor(
        public id: number = 0,
        public time: string = "",
        public channel: number = 0
    ) { }
}