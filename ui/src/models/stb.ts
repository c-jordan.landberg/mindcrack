// Stb log fields
export default class STB {
    constructor(
        public id: number = 0,
        public playout: string = "",
        public model: string = ""
    ) { }
}